function __js-utest(){
    if [[ ! ${1+x} ]]
    then
        paths=$(find ../ -type d -path '*/unit/javascript/core' | sed "{:q;N;s/\n/ /g;t q}")
    else
        paths="$@"
    fi
    echo -e "\n\nD8 Test Run:\n";
    js-utest.D8 $paths;
    echo -e "\n\n\nSpiderMonkey Test Run:\n";
    js-utest.SM $paths;
}

function __js-utest-module(){
    if test -d "../$1/test/unit/javascript/core" || test -d "../$1/unit/javascript/core"
    then
        js-utest "../$1/test/unit/javascript/core" "../$1/unit/javascript/core"
    else
        echo -e "\nUnknown Module: $1\n"
    fi
}

alias js-utest='__js-utest';
alias js-utest.Module='__js-utest-module';

alias js-utest.D8='../sfdc-test/tools/javascript/external/Engines/com.google_v8/3.12.3/d8 ../sfdc-test/tools/javascript/external/xUnit.js/xUnit.js.Console.js -- /dependency:../sfdc-test/tools/javascript/sfdc/ ../sfdc/config/jslibrary/sfdc/core/polyfills/';

alias js-utest.SM='../sfdc-test/tools/javascript/external/Engines/org.mozilla_spiderMonkey/1.8.5/js -e "var readLocal=read;read=function(path){return readLocal(path)};" ../sfdc-test/tools/javascript/external/xUnit.js/xUnit.js.Console.js /dependency:../sfdc-test/tools/javascript/sfdc/ ../sfdc/config/jslibrary/sfdc/core/polyfills/';j

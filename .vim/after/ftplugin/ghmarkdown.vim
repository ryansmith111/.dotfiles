" Marked.app - open current file in Marked app, which renders markdown preview
nnoremap <buffer> <leader><leader>m :silent !open -a "Marked 2" '%:p'<cr>
" set text wrapping in markdown files
" FIXME - disabled for now since it breaks other syntax
"autocmd FileType mkd setlocal fo=roqatl wm=5 wrap

" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1
" auto generate plantuml png on saving uml
autocmd BufWritePost silent make
" command to open the rendered .png file in google chrome
nnoremap <buffer> <leader><leader>m :silent !open -a "Google Chrome" '%:p:s?\.uml$?\.png?'<cr>

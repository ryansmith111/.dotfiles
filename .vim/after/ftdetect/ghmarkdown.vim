" use ghmarkdown filetype for markdown files
    "if exists("did_load_filetypes")
    "  finish
    "endif
	augroup filetypedetect
        au BufNewFile,BufRead *.md,*.markdown set filetype=ghmarkdown
	augroup END

"
" PRIMARY ELEMENTS:
"
" Example:
"  #quicktaskComment
"  quicktaskSection:
"   - quicktaskTask
"   * quicktaskNote
"   quicktaskNoteCont
"
" ORIGINAL
" Section Header
"highlight link quicktaskSection Identifier
""highlight quicktaskSection ctermfg=81 guifg=#66d9ef
""
"" task text. line prefixed by -
"highlight link quicktaskTask Normal
"" first line of a note. prefixed by *
"highlight link quicktaskNote Function
"" note contents. all non-prefixed lines after first note line
"highlight quicktaskNoteCont ctermfg=250 guifg=#E3E3E3
"" any comment lines. prefixed with #
"highlight link quicktaskComment Comment

" CUSTOM
highlight link quicktaskSection Identifier
"highlight quicktaskSection ctermfg=81 guifg=#66d9ef
"
" task text. line prefixed by -
highlight link quicktaskTask Normal
" first line of a note. prefixed by *
highlight link quicktaskNote String
" note contents. all non-prefixed lines after first note line
highlight link quicktaskNoteCont String
" any comment lines. prefixed with #
highlight link quicktaskComment Comment

"
" DECORATION:
"
" Text on a time annotations, including the @, and Start
highlight link quicktaskTimeNote Number
" DONE text annotation. This is just the text itself
highlight link quicktaskDone Statement

" Markers are tags in a task, like TODO, FIXME, ENH, AFB, HELD
highlight link quicktaskMarker Function

" constants are y|n|yes|not|true|false
highlight link quicktaskConstant Function

" date in an annotation
highlight link quicktaskDatestamp Comment

" time in an annotation
highlight link quicktaskTimestamp String

" the time on an unfinished task
highlight link quicktaskIncomplete Statement


"
" UNUSED:
"
" a ticket is a link to a Jira ticket
"highlight link quicktaskTicket Function
"highlight link quicktaskSnip  xxx links to Number
"highlight link quicktaskUsername xxx links to Special





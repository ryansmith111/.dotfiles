"hi OL1 guifg='white'      guibg='RoyalBlue4'
"hi OL2 guifg='RoyalBlue4'
"hi OL3 guifg='RoyalBlue2'
"hi OL4 guifg='SteelBlue3'
"hi OL5 guifg='RoyalBlue4'
"hi OL6 guifg='RoyalBlue2'
"hi OL7 guifg='RoyalBlue1'
"hi OL8 guifg='RoyalBlue4'
"hi OL9 guifg='RoyalBlue2'

hi link OL1 Type
hi link OL2 Function
hi link OL3 Number
hi link OL4 String
hi link OL5 Statement
hi link OL6 Identifier
hi link OL7 Constant
hi link OL8 Function
hi link OL9 Type
"colors for tags
"hi link outlTags Tag
hi link outlTags Todo

"color for body text
"hi link BT1 Comment
"hi link BT2 Comment
"hi link BT3 Comment
"hi link BT4 Comment
"hi link BT5 Comment
"hi link BT6 Comment
"hi link BT7 Comment
"hi link BT8 Comment
"hi link BT9 Comment

hi link BT1 Normal
hi link BT2 Normal
hi link BT3 Normal
hi link BT4 Normal
hi link BT5 Normal
hi link BT6 Normal
hi link BT7 Normal
hi link BT8 Normal
hi link BT9 Normal

"color for pre-formatted text
hi link PT1 Special
hi link PT2 Special
hi link PT3 Special
hi link PT4 Special
hi link PT5 Special
hi link PT6 Special
hi link PT7 Special
hi link PT8 Special
hi link PT9 Special

"color for tables
"hi TA1 guifg='DarkOrange2'
"hi TA2 guibg='DarkOrange2' guifg='white'
"hi TA3 guifg='DarkOrange2'
"hi TA4 guifg='DarkOrange2'
"hi TA5 guifg='DarkOrange2'
"hi TA6 guifg='DarkOrange2'
"hi TA7 guifg='DarkOrange2'
"hi TA8 guifg='DarkOrange2'
"hi TA9 guifg='DarkOrange2'
"
hi link TA1 Type
hi link TA2 Type
hi link TA3 Type
hi link TA4 Type
hi link TA5 Type
hi link TA6 Type
hi link TA7 Type
hi link TA8 Type
hi link TA9 Type

"color for user text (wrapping)
hi link UT1 Debug
hi link UT2 Debug
hi link UT3 Debug
hi link UT4 Debug
hi link UT5 Debug
hi link UT6 Debug
hi link UT7 Debug
hi link UT8 Debug
hi link UT9 Debug

"color for user text (non-wrapping)
hi link UB1 Special
hi link UB2 Special
hi link UB3 Special
hi link UB4 Special
hi link UB5 Special
hi link UB6 Special
hi link UB7 Special
hi link UB8 Special
hi link UB9 Special

"colors for folded sections
"hi link Folded Special
"hi link FoldColumn Type

"hi CursorLine guifg=fg
"colors for experimental spelling error highlighting
"this only works for spellfix.vim with will be cease to exist soon
hi link spellErr Error
hi link BadWord Todo

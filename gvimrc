
gui
set background=light
" LIGHT COLORSCHEMES - gui only
"colorscheme Cleanroom
"colorscheme newsprint
"colorscheme summerfruit256
"colorscheme onedark
"colorscheme leya
"colorscheme proton                      "light grey with colored bg and underline keyword highlights
"colorscheme Tomorrow
"let g:solarized_contrast="high"
"colorscheme solarized

" DARK COLORSCHEMES - gui only
"colorscheme getafe
"colorscheme molokai                      "FAV - Dark with bright red, blue, green
"colorscheme iceberg                     "FAV - this is blue in mvim, but in terminal bg is gray
"colorscheme flatland
syntax enable
"color sienna

"set guifont=Sauce\ Code\ Powerline\ Plus\ Nerd\ File\ Types:h16
set guifont=SauceCodePro\ Nerd\ Font:h16
" override title highlight color for ??????
"highlight Title  ctermfg=81 guifg=#66d9ef

" do not use guitabs
set guioptions-=e
set lines=50
set columns=90

" set default size of a new window
set lines=50
set columns=100


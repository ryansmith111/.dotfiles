# PATH Setup {{{

# get the short hostname so this can be shared across machines
export THISHOST=$(hostname -s)
# add home bin dir and ubin for my own scripts
export PATH=~/bin:~/ubin:$PATH

# Perforce config
export P4PORT=ssl:p4proxy.ashburn.soma.salesforce.com:1999

 #add brew bin to front of path to override system defaults
export PATH=/usr/local/bin:$PATH

#
export N_PREFIX="$HOME/n"; [[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"  # Added by n-install (see http://git.io/n-install-repo).

# define NODE_PATH so npm libraries will be picked up
export NODE_PATH='/usr/local/lib/node_modules'

# add android tools to path
export PATH=$PATH:~/dev/android-sdk-macosx/platform-tools:~/dev/android-sdk-macosx/tools

# nvm setup
#export NVM_DIR=~/.nvm
#if [ -f "$(brew --prefix nvm)/nvm.sh" ]; then
#    source $(brew --prefix nvm)/nvm.sh
#fi

## add npm module bin dir to path
#export PATH=$PATH:/usr/local/share/npm/bin

# add brew nginx dir to path
export PATH=$PATH:/usr/local/sbin
# add data-science-at-the-commandline-tools
export PATH=$PATH:~/dev/data-cli-tools/tools

# always use latest version of java 8
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)

# add jsctags from github.com/faceleg/doctorjs
export NODE_PATH=/usr/local/lib/jsctags/:$NODE_PATH

# set directory for global to put tags files
export MAKEOBJDIRPREFIX=~/obj

# PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
# }}}
# ack {{{
# allow per-directory ack configuration
export ACKRC=".ackrc"
# }}}
# Git {{{
#
# https://github.com/magicmonty/bash-git-prompt
#
# gitprompt configuration

# Set config variables first
GIT_PROMPT_ONLY_IN_REPO=0

GIT_PROMPT_FETCH_REMOTE_STATUS=0   # uncomment to avoid fetching remote status

# GIT_PROMPT_START=...    # uncomment for custom prompt start sequence
# GIT_PROMPT_END=...      # uncomment for custom prompt end sequence

# as last entry source the gitprompt script
# GIT_PROMPT_THEME=Custom # use custom .git-prompt-colors.sh
# GIT_PROMPT_THEME=Solarized # use theme optimized for solarized color scheme
#source ~/.bash-git-prompt/gitprompt.sh
if [ -f "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"  ]; then
    source "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"
fi

# bash completion
if [ -f `brew --prefix`/etc/bash_completion ]; then
    source `brew --prefix`/etc/bash_completion
fi

# git-flow completion
source ~/.dotfiles/git-flow-completion.bash
# }}}
# P4 and BLT config {{{

export P4EDITOR="vim"

export BLTCOLORIZE=true
export BLT_ACTIVE_RELEASE=main
#export BLT_ACTIVE_RELEASE=204/patch
export BLT_RELEASE_ROOT=$HOME/blt/app/$BLT_ACTIVE_RELEASE
export BLT_DATA=$HOME/blt/datamodel
export SFDC_DIR=$BLT_RELEASE_ROOT/core/sfdc
export SFDC_TEST_DIR=$BLT_RELEASE_ROOT/core/sfdc-test
# aura
export MVN_REPO_AURA_DIR=$BLT_RELEASE_ROOT/core/build/repository/org/auraframework/aura-framework
export AURA_SRC=~/dev/aura
export AURA_UPSTREAM_REMOTE=upstream
export AURA_LOCAL_MASTER_BRANCH=master
export AURA_LOCAL_WORKING_BRANCH=develop
#export AURA_LOCAL_WORKING_BRANCH=ryansmith/master-pending-historyservice-replace

export PYTHONPATH=$PYTHONPATH:~/dev/g4/lib/python

#source ~/blt/env.sh
# }}}
# ALIASES {{{
alias .prof='. ~/.profile'
# Dir navigation {{{
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias ~='cd ~'
alias ls='ls -laFG'
alias ll='tree --dirsfirst -ChFupDaLg'
# }}}
# blt and perforce aliases {{{
alias rt='cd $BLT_RELEASE_ROOT'

# scope p4 to clientspec
alias pf="p4 -c $THISHOST-blt"
alias p4b="p4 -c $THISHOST-blt"
alias p4g="p4 -d/Users/ryansmith/dev/p4-one-components -c $THISHOST-git-p4"

# }}}
# nginx aliases {{{
#
alias nxklog='rm ~/Development/webapp-core/local/log/*'
alias nxtlog='tail -f ~/Development/webapp-core/local/log/*.log'
alias nxreload='nginx -c ~/Development/webapp-core/build/nginx/rw_portal-local/nginx/nginx.conf -s reload'
alias nx='nginx -c ~/Development/webapp-core/build/nginx/rw_portal-local/nginx/nginx.conf'
alias nxq='nginx -c ~/Development/webapp-core/build/nginx/rw_portal-local/nginx/nginx.conf -s quit'
alias nxr='rt;clear;nxreload;nxklog;nxtlog'
alias nxn='rt;clear;nxq;nxklog;nx;nxtlog'
alias nxps='ps -aef | grep nginx'
# }}}
#git aliases {{{
#a most are in git config, but more complicated ones needing bash functions are here
alias g='git'
# brsync - list branches which are out of sync with their remote
alias brsync='git branch -a -v | sed -n -E -e "/(ahead|behind)/p"'

# get most recent aura tag in sfdc build mvn repo
function __aura-src-tag(){
    # list time and name of directory contents, sort by time, limit to top result, cut out the time
    # column, then just get the basename instead of the full path.
    dirname=$(stat -f "%m%t%N" $MVN_REPO_AURA_DIR/* | sort -rn | head -1 | cut -f2 | xargs basename);
    echo aura-framework-$dirname;
}
# just list the aura versions in the local sfdc build's mvn repo dir
function __aura-src-tags(){
    echo $MVN_REPO_AURA_DIR;
    # just lists the version dir. redirect output to /dev/null to quiet the console output
    pushd . > /dev/null;cd $MVN_REPO_AURA_DIR/;ls -A1td * | HEAD -10;popd > /dev/null;
    #find . -maxdepth 2 -path './aura-framework/*' -type d
}
alias auratag='__aura-src-tag';
alias auratags='__aura-src-tags';
alias aurart='cd $AURA_SRC';
#
# sync local aura git repo to the most recent tag in the sfdc build.
# Run this after blt --sync to get aura source to the same level as sfdc build before running build
#
function __aura-sync(){
    tag=$(auratag);
    # ensure all variables are non-empty - else the rebase command could end up with a different
    # meaning and cause all kinds of mess.
    if [[ -n $tag ]] && [[ -n $AURA_UPSTREAM_REMOTE ]]&& [[ -n $AURA_LOCAL_MASTER_BRANCH ]] && [[ -n $AURA_LOCAL_WORKING_BRANCH ]] && [[ -n $AURA_SRC ]]
    then
        echo Syncing $AURA_SRC git repo to tag: $tag;
        pushd . > /dev/null;
        pushd $AURA_SRC;
        # get updates from upstream - especially tags
        git fetch $AURA_UPSTREAM_REMOTE;
        # rebase local master branch to be based on the specified tag
        git rebase $tag $AURA_LOCAL_MASTER_BRANCH;
        # rebase working branch off master - now it is based off specified tag as well
        git rebase $AURA_LOCAL_MASTER_BRANCH $AURA_LOCAL_WORKING_BRANCH;
        popd > /dev/null;
    else
        echo 'auratag not found, or config not complete. no updates performed.'
    fi
}
alias aurasync='__aura-sync'
# }}}
# find|ack aliases {{{
# to find files in main source tree
#

function __jsack(){
    rt
    find core/**/components/** -name *.js -or -name *.cmp -or -name *.evt -or -name *.intf | ack $1
}
function __jack(){
    rt
    find core/**/java/src/** -name *.java | ack $1
}
# find java files
alias jack='__jack';
# find js and aura .cmp, .evt, .intf files
alias jsack='__jsack';
# }}}
# xunit aliases {{{
#

function __js-utest(){
    if [[ ! ${1+x} ]]
    then
        # if no path specified, assume pwd is the src root, so look up a dir for tests
        paths=$(find ../ -type d -path '*/unit/javascript/core' | sed "{:q;N;s/\n/ /g;t q}")
    else
        paths="$@"
    fi
    echo -e "\n\nD8 Test Run:\n";
    echo $paths
    js-utest.D8 $paths;
    #echo -e "\n\n\nSpiderMonkey Test Run:\n";
    #js-utest.SM $paths;
}

function __js-utest-module(){
    if test -d "../$1/test/unit/javascript/core" || test -d "../$1/unit/javascript/core"
    then
        js-utest "../$1/test/unit/javascript/core"
        #js-utest "../$1/test/unit/javascript/core" "../$1/unit/javascript/core"
    else
        echo -e "\nUnknown Module: $1\n"
    fi
}
alias js-utest='__js-utest';
alias js-utest.Module='__js-utest-module';
alias d8='$SFDC_TEST_DIR/tools/javascript/external/Engines/MacOS/com.google_v8/3.12.3/d8';

alias js-utest.D8='$SFDC_TEST_DIR/tools/javascript/external/Engines/MacOS/com.google_v8/3.12.3/d8 $D8_FLAGS $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js -- /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ ';

alias js-utest.SM='$SFDC_TEST_DIR/tools/javascript/external/Engines/MacOS/org.mozilla_spiderMonkey/1.8.5/js -e "var readLocal=read;read=function(path){return readLocal(path)};" $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ ';
# use functions instead of aliases - easier to compose without worrying about delayed alias
# resolution
function jsunit(){

    $SFDC_TEST_DIR/tools/javascript/external/Engines/MacOS/com.google_v8/3.12.3/d8 $D8_FLAGS $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js -- /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ /strict:false /verbose:true /coverage:false $@
}
function jsunit-debug(){

    /Users/ryansmith/.nvm/v0.10.32/bin/node-debug $D8_FLAGS $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ /strict:false /verbose:true /coverage:false $@
}
function jsunit.sm(){

$SFDC_TEST_DIR/tools/javascript/external/Engines/MacOS/org.mozilla_spiderMonkey/1.8.5/js -e "var readLocal=read;read=function(path){return readLocal(path)};" $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js  /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ /strict:false /verbose:true /coverage:false $@
}
#alias jsunit='d8 $D8_FLAGS $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js -- /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ /strict:false /verbose:false';
#alias jsunit='blt --build js-utest -Djstest.engine.os=MacOS -Djstest.modules= -Djstest.path=../ui-global/test/unit/javascript/core/ui-global/one/centerStageLibrary/StackTest.js';
#alias jsunit='blt --build js-utest -Djstest.engine.os=MacOS -Djstest.modules= -Djstest.path=../ui-global/test/unit/javascript/core/ui-global/one/actionsComposer/actionsComposerHelperTest.js';
alias unit='blt --build js-utest -Djstest.engine.os=MacOS -Djstest.modules= -Djstest.path=../ui-global/test/unit/javascript/core/ui-global/one/';

alias js-utest.aura.D8.='$SFDC_TEST_DIR/tools/javascript/external/Engines/MacOS/com.google_v8/3.12.3/d8 $D8_FLAGS $SFDC_TEST_DIR/tools/javascript/external/xUnit.js/xUnit.js.Console.js -- /dependency:$SFDC_TEST_DIR/tools/javascript/sfdc/ ';

alias auraunit='d8 /Users/ryansmith/dev/aura/aura-util/src/test/tools/xUnit/xUnit.js.Console.js -- /dependency:/Users/ryansmith/dev/aura/aura-util/src/test/tools/xUnit/dependencies/ /strict:false /verbose:false';
alias auraunitx='d8 /Users/ryansmith/dev/aura/aura-util/src/test/tools/xUnit/xUnit.js.Console.js -X -- /dependency:/Users/ryansmith/dev/aura/aura-util/src/test/tools/xUnit/dependencies/ /strict:false /verbose:false';
# }}}
# restart mDNSResponder {{{
#
alias dns='sudo launchctl unload -w /System/Library/LaunchDaemons/com.apple.mDNSResponder.plist && sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.mDNSResponder.plist'
alias netdown='sudo ifconfig en0 down'
alias netup='sudo ifconfig en0 up'
# }}}
# SASS watch files {{{
#

alias swatch='rt;sass -l -t expanded --watch  assets/sass/app.scss:build/css/app.css'
# }}}
# Other aliases {{{
#'alias delta='/Applications/DeltaWalker.app/Contents/MacOS/Deltawalker -mi $0 $1'
# these aliases are needed because sucky rvm environment. see https://github.com/carlhuda/janus/wiki/Rvm
# for a more detailed explanation
#alias vim='rvm system do /Applications/MacVim.app/Contents/MacOS/Vim $@'
#alias mvim='rvm system do /Applications/MacVim.app/Contents/MacOS/MacVim $@'
# }}}
# }}}
# functions {{{
#

# open man page as PDF in Preview - mac only. using ~/bin/pman instead
#pman() { man -t "$@" | open -f -a Preview; }

#}}}
# vim {{{
alias vim='/usr/local/Cellar/macvim/7.4-109/MacVim.app/Contents/MacOS/Vim'
alias vtemp='rm ~/.vim/_temp/*'
set -o vi
#}}}
# vim: foldmethod=marker

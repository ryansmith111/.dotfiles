#!/bin/bash
cd ~
ln -s ./.dotfiles/ackrc ~/.ackrc
ln -s ./.dotfiles/gvimrc ~/.gvimrc
ln -s ./.dotfiles/vimrc ~/.vimrc
ln -s ./.dotfiles/profile ~/.profile
ln -s ./.dotfiles/gitconfig ~/.gitconfig
ln -s ./.dotfiles/gitignore_global ~/.gitignore_global
ln -s ./.dotfiles/$HOSTNAME.vimrc ~/.$HOSTNAME.vimrc
#
# link .vim config dirs into ~/.vim
# check for existence first so we don't blow away the directory
#
cd .vim
ln -s ../.dotfiles/.vim/vimoutlinerrc vimoutlinerrc
if [ -h ./after ]
    then echo '.vim/after is already linked'
    else ln -s ~/.dotfiles/.vim/after after
fi
if [ -h ./autoload ]
    then echo '.vim/autoload is already linked'
    else ln -s ~/.dotfiles/.vim/autoload autoload
fi
if [ -h ./before ]
    then echo '.vim/before is already linked'
    else ln -s ~/.dotfiles/.vim/before before
fi
if [ -h ./syntax ]
    then echo '.vim/syntax is already linked'
    else ln -s ~/.dotfiles/.vim/syntax syntax
fi

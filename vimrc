""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Notes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Installation: {{{
" Vim Install:
" neocomplete requires lua, so install with:
" brew install vim --with-lua
" or
" brew install macvim --with-cscope --with-lua --HEAD
"
"
" NeoBundle Install:
"
" NeoBundle must be installed manually before this vimrc may be used on a new machine.
" Run the following:
" curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | sh
"
" }}}
" TO FIX: {{{
" - project loses syntax highlighting after something
" - markdown loses syntax highlighting after sourcing
" - airline changes to winbuftabs after changing windows
" - fix project buffer create/refresh errors
"
"}}}
" TO ADD: {{{
" - setup preview window and workflow to make it easy to explore files {{{
"   - preview from project window
"   - preview from unite buffer
"   - how to return from the preview window
"   - how to show preview window in an existing window layout
"   - how to cancel preview window and return to prior window layout
"   - how to open the previewed file in a non-preview window
"   }}}
" - easily work with components {{{
"   - unite search
"       - list components in project
"       - list components by namespace
"       - list base auraframework components, api
"   - MEGA tern integration to navigate components, find definition, find usages
"   - aura api
"       - tags
"       - generate vim doc for aura framework and our components
"   - window management for components
"       - opening component opens cmp, controller, helper, renderer, test
"           - open in new tab in two columns, one full height and other with hsplits. rotate windows
"           to change which is full height
"       - compare command to open a tab with hsplits for each component resource, each split
"       containing two vpslit windows with the resource for the LHS/RHS component
"  }}}
" - get unite configured for files and buffers {{{
"   <leader>
"       b  <unite> buffer
"       f  <unite> files
"       ff <unite> files - current dir
"          <unite> outline
"          <unite> projects
"       n  <unite> notes
"          <unite> cheatsheet
"          <unite> tasklists
"          <unite> java, js, css, main, patch,
"
"       Unite search sources
"           - search for components, when find it, open component dir in left column, cmp, css,
"               - components in current vim-project
"           - search for java source in main
"           - sfdc build release/patch/main
"           - java | js | cmp | css
"           - search projects, listing ~/dbox/projects when selected switch to that project, clearing all buffers
"           helper in right column
"           - search tasks, listing files in ~/dbox/tasks
"           - search for notes, by name or by content, with action to open
"           - git file history
"           - git commit history
"           - cheatsheet menu to list groups of keymappings
" }}}
" - project setup {{{
"       - generate project file from user-workspace.xml, or para config
"       - use conceal to hide the extra settings which clutter the list
"       - setup easy project switching. unite plugin to list dbox/.vim/projects and open
" }}}
"}}}
" TO TRY {{{
"   ballooneval to display balloon on mouseover text
"   rabbit-ui.vim displays messagebox, panel, grid, choices
"   sidepanel.vim - toggle display of multiple items in the sidebar, and move sidebar from left to
"   right
" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Environment Setup
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" enable aliases in commands-{{{
" this is needed to enable aliases to be invoked in commands
" currently disabled because is causes infinite looping
" let $BASH_ENV = "~/.profile"
" }}}
if has('vim_starting')
    " support host-specific vimrc {{{
    let s:host_vimrc = $HOME .'/.' . hostname() . '.vimrc'
    " }}}
    " load host-specific vimrc {{{
    " sets $HOST_* variables for use in the rest of the vimrc
    if filereadable(s:host_vimrc)
        execute 'source ' . s:host_vimrc
    endif
    " set runtimepath {{{
    " for vim plugins. adds my own ~/.vim and before and after dirs
    set runtimepath=$HOME/.vim,$HOME/.vim/before,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
endif
"}}}
" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NeoBundle
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NeoBundle init {{{
if has('vim_starting')
    if &compatible
        set nocompatible               " Be iMproved
    endif

    " Required:
    set runtimepath+=$HOME/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('/Users/ryansmith/.vim/bundle'))


" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'
" }}}
" Bundles to try {{{
" NeoBunde 'zhaocai/GoldenView.Vim' " Always have a nice view for vim split windows. uses golden ratio and text width
" NeoBundle 'zefei/vim-wintabs'    "see description of the options at: https://github.com/ap/vim-buftabline
" NeoBundle 'vim-scripts/utl.vim'
" NeoBundle 'Shougo/neomru.vim'
" NeoBundle 'Shougo/vimfiler.vim'
" NeoBundle 'mikelue/vim-maven-plugin'
" NeoBundle 'airblade/vim-rooter' - automatically change to project root when opening a file
" NeoBundle 'guns/vim-sexp' and tpope's mappings
" NeoBundle 'xolox/vim-easytags'
" NeoBundle 'ctags/cscope'
" NeoBundle 'vim-scripts/Merginal'                      "TUI for dealing with Git branches
" NeoBundle 'vim-scripts/closetag.vim'
" NeoBundle 'gabrielelana/vim-markdown'   "markdown with full featured editing and formatting " options
" NeoBundle 'tpope/vim-jdaddy'              "JSON manipulation and pretty-printing
" }}}
" Bundles {{{
"
" Add or remove your Bundles here:
"
" You can specify revision/branch/tag.
" NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }
"
"
" vimproc.vim - required for async {{{
NeoBundle 'Shougo/vimproc.vim', {
\ 'build' : {
\     'windows' : 'tools\\update-dll-mingw',
\     'cygwin' : 'make -f make_cygwin.mak',
\     'mac' : "make ARCHS='i386 x86_64' -f make_mac.mak",
\     'linux' : 'make',
\     'unix' : 'gmake',
\    },
\ }

" }}}
" File/Session Management {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
NeoBundle 'dahu/bisectly'                   "binary search to locate faulty plugins in vimrc
NeoBundle 'xolox/vim-misc'                  "common library for xolox plugins
NeoBundle 'xolox/vim-session'               "session management
NeoBundle 'tpope/vim-vinegar'               "enhancements to netrw
NeoBundle 'tpope/vim-dispatch'              "async build and test dispatcher
NeoBundle 'mhinz/vim-startify'              "fancy start screen for vim

"}}}
" File types/syntaxes {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"NeoBundle 'jelera/vim-javascript-syntax'
" enhanced js syntax highlights - needs to come before vim-javascript
"NeoBundleLazy 'jelera/vim-javascript-syntax', {'autoload':{'filetypes':['javascript']}}
NeoBundle 'pangloss/vim-javascript'
"NeoBundle 'plasticboy/vim-markdown'
"NeoBundle 'tpope/vim-markdown'
NeoBundle 'jtratner/vim-flavored-markdown'
NeoBundle 'aklt/plantuml-syntax'
NeoBundle 'Chiel92/vim-autoformat'
NeoBundle 'vim-syntastic/syntastic'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"}}}
" Interface {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
NeoBundle 'mileszs/ack.vim'
NeoBundle 'bruth/vim-newsprint-theme'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'NLKNguyen/papercolor-theme'
NeoBundle 'joshdick/onedark.vim'
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'Shougo/unite.vim'
NeoBundle 'Shougo/unite-outline'            "outline current buffer
"NeoBundle 'osyo-manga/unite-quickfix'       "view quickfix in unite buffer
"NeoBundle 'Shougo/unite-help'               "vim help plugin
"NeoBundle 'tsukkee/unite-tag'               "search tags
"NeoBundle 'sgur/unite-everything'           "searches all sources
"NeoBundle 'tacroe/unite-mark'
"NeoBundle 'Shougo/unite-session'
"NeoBundle 'Shougo/unite-build'
NeoBundle 'ujihisa/unite-colorscheme'       "changes the vim colorscheme
" neocomplete requires lua. install via:
"   brew install macvim --with-cscope --with-lua --HEAD
NeoBundle 'Shougo/neocomplete.vim'
"NeoBundle 'Shougo/javacomplete'
"NeoBundle ''
"NeoBundle 'bling/vim-airline'
NeoBundle 'zefei/vim-wintabs'               "trying this out
"NeoBundle 'bling/vim-bufferline'
NeoBundle 'majutsushi/tagbar'               "sidebar displaying ctags for current file
NeoBundle 'ervandew/supertab'               "tab insert completion
NeoBundle 'christoomey/vim-tmux-navigator'  "enables seamless movement between vim and tmux panes
NeoBundle 'ryanoasis/vim-webdevicons'       "icons for ft in airline, etc
NeoBundle 'Lokaltog/vim-easymotion'         "quick two key movement anywhere
NeoBundle 'rgarver/Kwbd.vim'                "keep window, buffer delete
"NeoBundle 'fholgado/minibufexpl.vim'       "buffer explorer rendering as tabs

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"}}}
" Editing Tools {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
NeoBundle 'tpope/vim-surround'              "quoting/parenthesizing made simple. CRUD surrounding parens, brackets, quotes, etc
NeoBundle 'tpope/vim-unimpaired'            "brace matching
NeoBundle 'tpope/vim-repeat'                "allow . to repeat more things
NeoBundle 'chrisbra/NrrwRgn'                "focus on a region of a buffer, and make the rest inaccessible
NeoBundle 'vim-scripts/DeleteTrailingWhitespace'
NeoBundle 'vim-scripts/ShowTrailingWhitespace'
NeoBundle 'sjl/gundo.vim'                   "visualize undo tree
NeoBundle 'Raimondi/delimitMate'            "auto-close delimiters
NeoBundle 'scrooloose/nerdcommenter'        "manage commenting/uncommenting
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'honza/vim-snippets'
"NeoBundle 'ultisnips'
NeoBundle 'michaeljsmith/vim-indent-object' "defines a new text object for lines of code at same indent level
NeoBundle 'terryma/vim-multiple-cursors'    "sublime-like multi-select
NeoBundle 'godlygeek/tabular'               "tool to align text
NeoBundle 'dhruvasagar/vim-table-mode'      "creates text tables automatically

"}}}
" Applications {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
NeoBundle 'vim-scripts/project.tar.gz'       "IDE-like project sidebar, highly flexible
"NeoBundle 'fmoralesc/vim-pad'               "<leader>Esc to list notes
"NeoBundle 'aaronbieber/vim-quicktask'       "ft to manage task lists
NeoBundle 'tpope/vim-fugitive'              "git integration
NeoBundle 'vimoutliner/vimoutliner'         "text based outliner for .otl files
NeoBundle 'mattn/calendar-vim'              "text based calendar

" }}}
" }}} " NeoBundle wrapup {{{
" Required:
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

" }}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basics: {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set visualbell                      " stop vim from beeping
set mouse=a                         " enable all mouse modes in terminal
set ttymouse=sgr                    " enable mouse tracking beyond column 233 in vim and tmux
"set nocompatible                    " disable vi compatibility
set laststatus=2                    " always show the statusline
set showcmd                         " show input of incomplete command, e.g. "y23dd will display before the last d completes the command
set wildmenu                        " show possible command completions on status line
"wildmode=longest,list,full
set encoding=utf8                   " needed to show unicode glyphs (e.g. for powerline)
set t_Co=256                        " explicitly tell Vim that the terminal supports 256 colors
set shellpipe=&>                    " override where shell output is piped, so executing system commands doesn't flash screen, e.g. Ack
set incsearch hlsearch smartcase    " show search matches as you type and highlight them (clear with :noh)
set virtualedit=block,onemore       " allow cursor to move into blank space in visual block mode, and one char past the end always
set splitright splitbelow           " new splits appear below or to the right of current

set modelines=1

" sets line numbering to be relative to the current line
" set relativenumber "this was too slow to use at the time I tried it
set number

"set guitablabel=%N\ %t



"}}}
" Colors: {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set background=light
" LIGHT COLORSCHEMES
"colorscheme newsprint
colorscheme summerfruit256
"colorscheme PaperColor
"colorscheme lucius
"
" DARK COLORSCHEMES
"set background=dark
"colorscheme onedark
"
"colorscheme apprentice                  "FAV
"colorscheme darth                       "FAV
"colorscheme base16-default
"colorscheme distinguished
"colorscheme flatlandia                  "FAV - gui theme based on sublime theme.
"colorscheme iceberg                     "FAV - this is blue in mvim, but in terminal bg is gray
"colorscheme lizard256
"colorscheme molokai                      "FAV - Dark with bright red, blue, green
"colorscheme solarized
"colorscheme sorcerer
"colorscheme Slate
"
" LIGHT && DARK
"colorscheme Tomorrow-Night-Eighties
"colorscheme Tomorrow
"colorscheme Tomorrow-Night
"
"set background=light
"set background=dark
"
" lucius has multiple variants: LuciusWhite/Light/Dark/Black
"colorscheme lucius
"LuciusDark
"colorscheme pencil

syntax enable
"}}}
" colorscheme overrides {{{
" To view current highlighting in use, execute
"   :hi
" To view a single hilighting group, execute
"   :hi {groupname}
" To also list the last place the value was set, prefixe with verbose:
" : verbose hi
map <F10> :echo "verbose hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
function! ColorOverrides() " {{{
    echo "fixing colorscheme"
    hi link htmlTag                     Function
    hi link htmlEndTag                  Identifier
    hi link htmlArg                     Type
    hi link htmlTagName                 htmlStatement
    hi link htmlSpecialTagName          Exception
    hi link htmlValue                     String
    hi link htmlSpecialChar             Special

    hi link htmlH1                      Title
    hi link htmlH2                      htmlH1
    hi link htmlH3                      htmlH2
    hi link htmlH4                      htmlH3
    hi link htmlH5                      htmlH4
    hi link htmlH6                      htmlH5
    hi link htmlHead                    PreProc
    hi link htmlTitle                   Title
    hi link htmlBoldItalicUnderline     htmlBoldUnderlineItalic
    hi link htmlUnderlineBold           htmlBoldUnderline
    hi link htmlUnderlineItalicBold     htmlBoldUnderlineItalic
    hi link htmlUnderlineBoldItalic     htmlBoldUnderlineItalic
    hi link htmlItalicUnderline         htmlUnderlineItalic
    hi link htmlItalicBold              htmlBoldItalic
    hi link htmlItalicBoldUnderline     htmlBoldUnderlineItalic
    hi link htmlItalicUnderlineBold     htmlBoldUnderlineItalic
    hi link htmlLink                    Underlined

    " adding colorscheme specific overrides
    "if has("gui_running")
    "    if (g:colors_name =~ "leya")
    "        hi Folded        guibg=#001336 guifg=#003DAD gui=none
    "        hi CursorLine    guibg=#000000 ctermbg=Black cterm=none

    "    elseif (g:colors_name =~ "256-jungle")
    "        hi CursorLine    guibg=#000000 ctermbg=Black cterm=none

    "    elseif (g:colors_name =~ "")
endfunction
"}}}"
augroup mycolorschemes
    au!
    au ColorScheme * silent call ColorOverrides()
augroup END
"highlight Title  ctermfg=81 guifg=#66d9ef
"highlight Title ctermfg=231 ctermbg=31 guifg=#ffffff guibg=#0087af
"highlight projectDirectory ctermfg=231 ctermbg=31 guifg=#ffffff guibg=#0087af

"highlight column 100 as a right margin
set colorcolumn=100

" enable highlighting the cursors current line and/or column
set cursorline
"highlight CursorLine ctermbg=254
"highlight CursorLine ctermfg=231 ctermbg=31 guifg=#ffffff guibg=#0087af
"set cursorcolumn
"hi CursorLine   cterm=NONE ctermbg=234 ctermfg=NONE

" override color schemes search highlight
"hi Search cterm=NONE ctermfg=black ctermbg=yellow

" highlights background in subtle red for text beyond colomn 80
"highlight OverLength ctermbg=red ctermfg=white guibg=#592929
"match OverLength /\%>100v.\+/
" /\%81v.\+/

"}}}
" Spaces & Tabs {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" formatoptions:
" c - autowrap comments
" r - auto insert comment leader after <enter>
" o - auto insert comment leader after 'o' or 'O' in normal mode
" q - allow formatting of comments with 'gq'
" l - don't autobreak a longline when entering insert mode on it
" t - autowrap text at textwidth
" j - when joining two lines with trailing comment, merge into single comment
" b - only auto break line if blank space entered at or before margin
set formatoptions=croql

" set indenting options`
filetype indent on
set smartindent
set autoindent
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set textwidth=100
set wrapmargin=100
set linebreak
set breakindent                      " wrapped lines will maintain visual indentation
set breakindentopt=min:20,shift:8    " shift specifies additional indent chars after wrap
set nowrap                  " nowrap by default, enable per filetype
set backspace=indent,eol,start

"}}}
" Backups {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Backup and Swap files - store in common temp dir instead of with src
" make sure our temp dir is created
" Automatically create .backup directory, writable by the group.
"if !filewritable("~/.vim/temp//")
    "silent execute '!umask 002; mkdir .backup'
    "silent execute '!mkdir ~/.vim/temp/'
"endif
"if !isdirectory('~/.vim/temp')
    "silent execute '!mkdir ~/.vim/temp'
"endif
" automatically blow the files away when we start
set backupdir=~/.vim/temp//
set directory=~/.vim/temp//
"if !empty(~/.vim/temp//)

if !empty(globpath(&backupdir, '*'))
    silent execute '!rm /Users/ryansmith/.vim/temp/*'
endif

" }}}
" Folding: {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set foldmethod=syntax
set foldlevel=100           " don't fold anything initially
set foldcolumn=0            " mostly as a left margin for the buffer contents
syn region foldBraces start=/{/ end=/}/ transparent fold keepend extend
"-------INDENT - worked ok
"set foldmethod=indent
"set foldlevel=2
"--------MARKERS - only useful if you close manually
"set foldmarker={,} " only use with a high foldlevel
"set foldmethod=marker
"set foldlevel=100


"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" KeyMappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Movement {{{

" Ctrl+motion changes window focus
" <C-W> <C-W> cycles through the windows
" temp disabled to try tmux-navigator
noremap <C-J>     <C-W>j
noremap <C-K>     <C-W>k
noremap <C-H>     <C-W>h
noremap <C-L>     <C-W>l

" Move rows - doesn't work in terminal
noremap <C-M-down> ddp
noremap <C-M-up> dd<up>P

" enable Indent/Unindent with Tab/Shift-Tab in all modes
inoremap <S-Tab> <Esc><<i
nnoremap <Tab> >>
nnoremap <S-Tab> <<
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

" select all
noremap <c-a> ggVG

" TAB PAGES
"gt | fn+Ctrl+DOWN  = next tab
"gT | fn+Ctrl+UP = previous tab
" {i}gt = go to tab in position i
noremap gt :tabnext<cr>
noremap gT :tabprevious<cr>
"set showtabline=2               " File tabs allways visible
":nmap <C-S-tab> :tabprevious<cr>
":nmap <C-tab> :tabnext<cr>
":nmap <C-t> :tabnew<cr>
":map <C-t> :tabnew<cr>
":map <C-S-tab> :tabprevious<cr>
":map <C-tab> :tabnext<cr>
":map <C-w> :tabclose<cr>
":imap <C-S-tab> <ESC>:tabprevious<cr>i
":imap <C-tab> <ESC>:tabnext<cr>i
":imap <C-t> <ESC>:tabnew<cr>

" }}}
" General {{{
"
" easier to exit input mode
inoremap jj <Esc>
inoremap jk <Esc>

" disable keys most often typed by accident
nnoremap <F1> <nop>
nnoremap Q <nop>
nnoremap K <nop>

" delete buffer but keep the window
"nnoremap <C-W>!  <Plug>Kwbd
"command! Bd <Plug>Kwbd
"nmap Bd  <Plug>Kwbd

" fix autoclose problems with mustache-style templates
"inoremap {{ {{}}<Esc>hi

" TODO: find a better mapping for these
"turns of highlights from last search
noremap <F3> :noh<CR>
"reload current file
noremap <F5> :e %<CR>
"toggle showing non-printables
noremap <F6> :set invlist<CR>

" After whitespace, insert the current directory into a command-line path
cnoremap <expr> <C-P> getcmdline()[getcmdpos()-2] ==# ' ' ? expand('%:p:h') : "\<C-P>"

" }}}
" Leader Mappings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" TODO: add: (from Janus)
" Write a privileged file with :SudoW or :SudoWrite, it will prompt for sudo password when writing
" gw swaps the current word with the following word
" <C-W>! invokes kwbd plugin; it closes all open buffers in the open windows but keeps the windows
" open

let mapleader = ","
let maplocalleader = "\\"
"let maplocalleader = " "
" NOTE: localleader mappings are for filtype-specific buffers, and are defined in the "Plugin Settings"
" section within the specific plugin section


" cd to the directory containing the file in the buffer
nmap <silent><leader>cd :lcd %:h<CR>
"
" " Create the directory containing the file in the buffer
nmap <silent><leader>md :!mkdir -p %:p:h<CR>

"noremap <Leader><Leader>r :! jsunit ~/blt/app/main/core/ui-global-components/test/unit/javascript/core/ui-global-components/one/centerStageLibrary/RouteTest.js<CR>
"
" Some helpers to edit mode
" http://vimcasts.org/e/14
"
" expands to the path of the directory containing the current file
cnoremap %% <C-R>=fnameescape(expand('%:h')).'/'<cr>
" edit a file in the same directory as current
" TODO change this to open Unite file list
map <leader>ew :e %%
map <leader>es :sp %%
map <leader>ev :vsp %%
map <leader>et :tabe %%

" Underline the current line with '=' or '-'
" (for a line of n chars, say 80, just type 80i<char><CR> to repeat )
"nmap <silent> <leader>ul :t.<CR>Vr-
"nmap <silent> <leader>uL :t.<CR>Vr=

" set text wrapping toggles
" use cow instead from tpope
"nmap <silent> <leader>wt :set invwrap<CR>:set wrap?<CR>

" find (merge) conflict markers
nmap <silent> <leader>fc <ESC>/\v^[<=>]{7}( .*\|$)<CR>

" Toggle hlsearch with <leader>hs TODO change to :noh ??
"nmap <leader>hs :set hlsearch! hlsearch?<CR>
" use coh instead from tpope
nmap <leader>hs :noh<CR>

" format the entire file
nnoremap <leader>fef :normal! gg=G``<CR>

"
" Unite
"
"nnoremap <leader>r :<C-u>Unite -start-insert file_rec<CR>
" list files
nnoremap <leader>u :<C-u>Unite source<CR>
nnoremap <leader>f :<C-u>Unite -start-insert file_rec<CR>
nnoremap <leader>v :<C-u>Unite -start-insert file_rec/async:!<CR>
" list buffers
nnoremap <leader>b :<C-u>Unite -start-insert buffer<CR>
" outline the current buffer
nnoremap <leader>o :<C-u>Unite outline<CR>
" TODO: add (t)asks, (p)rojects, (n)otes

" Ack
noremap <Leader>a :silent Ack<space>

" Gundo
nnoremap <leader>g :GundoToggle<CR>

" session.vim
noremap <leader>so :OpenSession
" conflicted with another plugin's default mappings. vim-pad, I think
"noremap <leader>ss :SaveSession<CR>
noremap <leader>sc :CloseSession<CR>

"
" LEADER LEADER mappings for special actions
"
"
" Notes
"nnoremap <leader><leader>n :Note<space>
nnoremap <leader><leader>n :Unite file_rec/async:~/dbox/notes<cr>
"nnoremap <leader><leader>m :Unite file_rec/async:~/blt/app/main/core<cr>
" Task lists
"nnoremap <leader><leader>t :Task<space>
" open today.md
nmap <silent><leader>T :e ~/dbox/notes/today.otl<CR>
" Project
nnoremap <silent><Leader>p :Project<CR>
nnoremap <Leader><Leader>p :Prj<space>

" TagBar
noremap <leader><leader>t :TagbarToggle<cr>



" source and edit vimrc in a vertical split
"so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif

nnoremap <leader><leader>v :tabe $MYVIMRC<cr>:vsplit $MYGVIMRC<cr>
nnoremap <leader><leader>s :source $MYVIMRC<cr>
nnoremap <leader><leader>g :source $MYGVIMRC<cr>

"}}}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Unite settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" use fuzzy matcher by default
call unite#filters#matcher_default#use(['matcher_fuzzy'])

" displays just the last path segment of a directory instead of the full path
call unite#filters#converter_default#use(['converter_tail_abbr'])
"call unite#filters#converter_default#use(['converter_relative_word'])
"call unite#filters#converter_default#use(['converter_relative_word'])
"call unite#filters#converter_default#use(['converter_tail'])
"call unite#filters#converter_default#use(['converter_relative_abbr'])

"call unite#custom#source('file_rec', 'matchers',
		"\ ['converter_relative_word', 'matcher_default']),

" Default profile settings
call unite#custom#profile('default', 'context', {
\   'no-split' : 1,
\   'source__directory': '~/blt/app/main/core'
\ })
" note: to create a per-source profile, do the following:
"call unite#custom#profile('source/components', 'context', {
" For ack.
if executable('ack')
    let g:unite_source_grep_command = 'ack'
    let g:unite_source_grep_default_opts = '-i --no-heading --no-color -k -H'
    let g:unite_source_grep_recursive_opt = ''
endif

" display in current buffer instead of in a split
"
" enable auto-preview
"
" default to quick match

"unite-options-vertical
"-vertical
"unite-options-winwidth
"
"}}}
" Unite source aliases {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" define aliases for custom scoped file sources
let g:unite_source_alias_aliases = {
\   'notes' : {
\     'source': 'file_rec',
\     'args': '~/dbox/notes',
\   },
\   'componentsug': {
\     'source': 'directory',
\     'args': '~/blt/app/main/core/ui-global-components/components/one',
\     'context': {'source__directory': '~/blt/app/main/core'}
\   },
\   'componentsfp': {
\     'source': 'directory',
\     'args': '~/blt/app/main/core/ui-force-private/components/force',
\     'context': {'source__directory': '~/blt/app/main/core'}
\   },
\   'main': {
\     'source': 'file_rec',
\     'args': '~/blt/app/main/core/ui-**/*'
\   },
\   'components': {'source': 'componentsug componentsfp'}
\ }
"}}}
" Unite menu definitions {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" menus are defined in a dictionary, key is name, values are:
"   myMenu = {
"     candidates: LIST|DICTIONARY, the items to show in the menu
"     command_candidates: LIST|DICTIONARY,
"     map: FUNCTION ->
"   }
"
let g:unite_source_menu_menus = {}
let g:unite_source_menu_menus.vim = {
      \     'description' : 'Vim mappings',
      \ }
let g:unite_source_menu_menus.vim.command_candidates = [
      \   ['<LEADER>'            , 'echo "vim"'],
      \   ['u - unite'           , 'echo "vim"'],
      \   ['<LEADER><LEADER>'    , 'echo "vim"'],
      \   ['--- unite ---'       , 'echo "vim"'],
      \   ['b - buffers (unite)' , 'echo "vim"'],
      \   ['n - notes (unite)'   , 'echo "vim"'],
      \   ['--- vimrc ---'       , 'echo "vim"'],
      \   ['v - edit .vimrc'     , 'echo "vim"'],
      \   ['s - source .vimrc'   , 'echo "vim"'],
      \   ['p - select project'  , 'echo "vim"'],
      \   ['<esc> - list pads'   , 'echo "vim"'],
      \ ]

nnoremap <silent> sm  :<C-u>Unite -vertical menu:vim<CR>
"}}}
" Unite keymappings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" got these from here: http://www.codeography.com/2013/06/17/replacing-all-the-things-with-unite-vim.html
" -no-split configures unite to open in the current buffer window
nnoremap <leader>m :<C-u>Unite -no-split -no-resize -buffer-name=directory  -create -auto-preview  directory:~/blt/app/main/core/ui-global-components/components/one<cr>
nnoremap <leader>t :<C-u>Unite -no-split -buffer-name=files   -start-insert file_rec/async:!<cr>
nnoremap <leader>f :<C-u>Unite -no-split -buffer-name=files   -start-insert file<cr>
nnoremap <leader>r :<C-u>Unite -no-split -buffer-name=mru     -start-insert file_mru<cr>
nnoremap <leader>o :<C-u>Unite -no-split -buffer-name=outline -start-insert outline<cr>
nnoremap <leader>y :<C-u>Unite -no-split -buffer-name=yank    history/yank<cr>
nnoremap <leader>b :<C-u>Unite -no-split -buffer-name=buffer  buffer<cr>
nnoremap <leader><leader>u <Plug>(unite_redraw)
nnoremap <leader><leader>U <Plug>(unite_restart)
"}}}
" Unite example customizing source output {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" example to customize output of source
" from https://github.com/Shougo/unite.vim/issues/412
" let s:filters = {
"\   "name" : "my_converter",
"\}

"function! s:filters.filter(candidates, context)
"    for candidate in a:candidates
"        let bufname = bufname(candidate.action__buffer_nr)
"        let filename = fnamemodify(bufname, ':p:t')
"        let path = fnamemodify(bufname, ':p:h')

"        " Customize output format.
"        let candidate.abbr = printf("[%s] %s", filename, path)
"    endfor
"    return a:candidates
"endfunction

"call unite#define_filter(s:filters)
"unlet s:filters


"call unite#custom#source('buffer', 'converters', 'my_converter')

"}}}
" WinTabs settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:wintabs_display='statusline'
let g:wintabs_ui_sep_leftmost=' '
let g:wintabs_ui_sep_inbetween='|'
let g:wintabs_ui_sep_rightmost='|'

let g:wintabs_ui_active_left='['
let g:wintabs_ui_active_right=']'
let g:wintabs_ui_active_higroup='Function'
"}}}
" Airline settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:webdevicons_enable = 1
"syntax enable

let g:airline_powerline_fonts = 1
"let g:airline_theme='murmur'
"let g:airline_theme='simple'
let g:airline_theme='mylight'
"let g:airline_theme='light'
"let g:airline_theme='dark'
"let g:airline_theme='base16'
"let g:airline_theme='molokai'
"let g:airline_theme='powerlineish'
"let g:airline_theme='wombat'
"let g:airline_theme='sol'
" show buffers in tabline
"let g:airline#extensions#tabline#enabled = 1
" restyle separators in tabline to vertical separator
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '|'

"don't show mixed whitespace errors in statusline
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#whitespace#show_message = 0
" override theme
"let g:airline_theme_patch_func = 'AirlineThemePatch'
"function! AirlineThemePatch(palette)
"  if g:airline_theme == 'light'
"    echo a:palette.normal
"    for colors in values(a:palette.normal)
"        echo colors
"        "for items in colors
"            "echo items
"        "endfor
"      "let colors[1] = '#333'
"    endfor
"  endif
"endfunction
"}}}
" Bufferline settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" disable buffers echo'd onto command line
let g:bufferline_echo = 0
" highlight even if only one buffer
let g:bufferline_solo_highlight = 1

"}}}
" Ack settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" map <Leader>a :Ack --ignore-dir=dist --ignore-dir=build --ignore-dir=node_modules --ignore-dir=assets/js<space>
" open in bottom right with 30 lines
let g:ack_qhandler = "botright copen 30"
" highlight searched term
let g:ackhighlight = 1
" auto-fold results
"let g:ack_autofold_results = 1


"}}}
" Netrw settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:netrw_browse_split=4               " when pressing <cr> open  the file like 'P' - previous window
let g:netrw_altv=1                  " split belowright
let g:netrw_preview=1               " split vertically when previewing files
let g:netrw_alto=0                  " split belowright
let g:netrw_liststyle=3             " make 'tree' listing style the default
let g:netrw_winsize=30              " directory listing will only use 30% of available columns, preview window gets the rest
let g:netrw_list_hide='.*\.swp$,target,test,.*\.class$'


"}}}
" DelimitMate settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" DelimitMate autocloses delimiters
let delimitMate_autoclose = 1
let delimitMate_expand_space = 1
let delimitMate_expand_cr = 2
let delimitMate_jump_expansion = 1
"}}}
" Session settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set sessionoptions-=help            " don't persist help windows
set sessionoptions-=tabpages        " just save current tab
let g:session_autoload = 'no'
let g:session_autosave = 'no'
let g:session_default_to_last = 'true'
let g:session_verbose_messages = 'false'

"}}}
" NeoComplete settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:neocomplete#enable_at_startup = 1
" disable fuzzy completion and use head completion, to avoid issue of a fuzzy match completing and
" not allowing continued typing from the last char typed.
let g:neocomplete#enable_fuzzy_completion = 0
" set to 1 to invalidate autocompletion and use manual completion (<C-x><C-u>)
let g:neocomplete#disable_auto_complete = 1
" when enabled, this updates the candidates list after user input
let g:neocomplete#enable_insert_char_pre = 1
" enable Tab to manually autocomplete
inoremap <expr><Tab>  neocomplete#start_manual_complete()
" Close popup by <Space>.
inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
"}}}
" NeoSnippets settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
"C-k to select-and-expand a snippet from the Neocomplcache popup (Use C-n and C-p to select it).
"C-k can also be used to jump to the next field in the snippet.
"Tab to select the next field to fill in the snippet.
"

" Plugin key-mappings.
 imap <C-k>     <Plug>(neosnippet_expand_or_jump)
 smap <C-k>     <Plug>(neosnippet_expand_or_jump)
 xmap <C-k>     <Plug>(neosnippet_expand_target)
"
" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif

"}}}
" NERDCommenter settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    let g:NERDCustomDelimiters = {
        \ 'plantuml': { 'left': "'" }
    \ }

"}}}
" Project settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:proj_window_width=40
let g:proj_flags="imstc"

" function to execute a p4 command on a file in the project buffer
function! s:DoP4(cmd)
    let name=Project_GetFname(line('.'))
    let dir=substitute(name, '\(.*\)/.*', '\1', 'g')
    exec 'cd '.dir
    exec "!".a:cmd.' '.Project_GetFname(line('.'))
    cd -
endfunction
augroup projectconfig
    au!
    autocmd BufRead *.vimproject nmap <buffer> <silent> <localleader>pa :call <SID>DoP4("p4 -c " . $HOST_P4CLIENT . " add")<CR>
    autocmd BufRead *.vimproject nmap <buffer> <silent> <localleader>pe :call <SID>DoP4("p4 -c " . $HOST_P4CLIENT . " edit")<CR>
augroup END

"}}}
" Gundo settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" options (with default value listed):
"let g:gundo_width = 45
"let g:gundo_preview_height = 15
"let g:gundo_right = 0
" give preview window full width at bottom
"let g:gundo_preview_bottom = 0
" disable help text on the graph window
let g:gundo_help = 0

"}}}
" DeleteTrailingWhitespace settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" automatically delete on save buffer
let g:DeleteTrailingWhitespace_Action = 'delete'
let g:DeleteTrailingWhitespace = 1
"}}}
" Syntastic settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" syntastic was causing temporary ui freezes, so set to passive
let g:syntastic_mode_map = { 'mode': 'active',
    \ 'active_filetypes': [],
    \ 'passive_filetypes': ['html','javascript','java'] }

"}}}
" Markdown settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vim_markdown_initial_foldlevel=1
"}}}
" Table-Mode settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" GFM style tables
let g:table_mode_corner="|"

"}}}
" Calendar settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:calendar_filetype="votl"
let g:calendar_diary=$HOME.'/dbox/diary'
let g:calendar_diary_extension=".otl"
let g:calendar_monday = 1

"}}}
" Autogroups {{{
augroup myvimrc
    au!
    au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc,vimoutlinerrc so $MYVIMRC
augroup END

augroup configgroup
    " this clears the group, so we don't end up with duplicates
    autocmd!
    autocmd BufNewFile,BufRead *.quicktask setfiletype quicktask

    " map aura component file extensions to xml
    autocmd BufNewFile,BufRead *.app set filetype=xml
    autocmd BufNewFile,BufRead *.cmp set filetype=xml
    autocmd BufNewFile,BufRead *.evt set filetype=xml
    autocmd BufNewFile,BufRead *.intf set filetype=xml
    autocmd BufNewFile,BufRead *.lib set filetype=xml


    " don't show trailing whitespace in the unite buffer
    autocmd FileType unite call ShowTrailingWhitespace#Set(0,1)

    " Java ant make  enablement
    autocmd BufRead *.java set efm=%A\ %#[javac]\ %f:%l:\ %m,%-Z\ %#[javac]\ %p^,%-C%.%#
    autocmd BufRead set makeprg=ant\ -find\ build.xml

    " auto generate plantuml png on saving uml
    autocmd BufWritePost *.uml silent make
    " Define nginx conf file type
    "au BufRead,BufNewFile nginx*.conf set ft=nginx
augroup END

"augroup markdown
"    au!
"    au BufNewFile,BufRead *.md,*.markdown setlocal filetype=ghmarkdown
"    " Marked.app - open current file in Marked app, which renders markdown preview
"    au FileType ghmarkdown nnoremap <buffer> <leader><leader>m :silent !open -a "Marked 2" '%:p'<cr>
"    " set text wrapping in markdown files
"    " FIXME - disabled for now since it breaks other syntax
"    "autocmd FileType mkd setlocal fo=roqatl wm=5 wrap
"augroup END

augroup uniteconfiggroup
    " this clears the group, so we don't end up with duplicates
    autocmd!
    autocmd FileType unite call s:unite_my_settings()
    function! s:unite_my_settings()"{{{
        " don't show trailing whitespace in the unite buffer

        call ShowTrailingWhitespace#Set(0,1)

      " Overwrite settings.
      imap <buffer> jj      <Plug>(unite_insert_leave)
      "imap <buffer> <C-w>     <Plug>(unite_delete_backward_path)

      imap <buffer><expr> j unite#smart_map('j', '')
      imap <buffer> <TAB>   <Plug>(unite_select_next_line)
      imap <buffer> <C-w>     <Plug>(unite_delete_backward_path)
      imap <buffer> '     <Plug>(unite_quick_match_default_action)
      nmap <buffer> '     <Plug>(unite_quick_match_default_action)
      imap <buffer><expr> x
              \ unite#smart_map('x', "\<Plug>(unite_quick_match_choose_action)")
      nmap <buffer> x     <Plug>(unite_quick_match_choose_action)
      nmap <buffer> <C-z>     <Plug>(unite_toggle_transpose_window)
      imap <buffer> <C-z>     <Plug>(unite_toggle_transpose_window)
      imap <buffer> <C-y>     <Plug>(unite_narrowing_path)
      nmap <buffer> <C-y>     <Plug>(unite_narrowing_path)
      nmap <buffer> <C-j>     <Plug>(unite_toggle_auto_preview)
      nmap <buffer> <C-r>     <Plug>(unite_narrowing_input_history)
      imap <buffer> <C-r>     <Plug>(unite_narrowing_input_history)
      nnoremap <silent><buffer><expr> l
              \ unite#smart_map('l', unite#do_action('default'))

      let unite = unite#get_current_unite()
      if unite.profile_name ==# 'search'
        nnoremap <silent><buffer><expr> r     unite#do_action('replace')
      else
        nnoremap <silent><buffer><expr> r     unite#do_action('rename')
      endif

      nnoremap <silent><buffer><expr> cd     unite#do_action('lcd')
      nnoremap <buffer><expr> S      unite#mappings#set_current_filters(
              \ empty(unite#mappings#get_current_filters()) ?
              \ ['sorter_reverse'] : [])

      " Runs "split" action by <C-s>.
      imap <silent><buffer><expr> <C-s>     unite#do_action('split')
    endfunction"}}}
augroup END
" }}}
" Custom commands and functions {{{
"
" diff a buffer with the file it was loaded from
silent command! DiffOrig vert new | set bt=nofile | r ++edit # | 0d_  | diffthis | wincmd p | diffthis

" open a split which lists all syntax highlighting colors for the current buffer/colorscheme
" :hi - list all hightlight groups and their color values
" :hi Statement - to view just the specified highlight group
silent command! ShowColors runtime syntax/hiTest.vim

" auto align tables whenever inserting a |
inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a
function! s:align()
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
    let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
    let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
    Tabularize/|/l1
    normal! 0
    call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction

"identify the syntax highlighting group used at the current cursor position
" see http://vim.wikia.com/wiki/Identify_the_syntax_highlighting_group_used_at_the_cursor
" for details and other options
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" Note command and autocomplete {{{
let g:my_notes_dir='~/dbox/notes/'
silent command! -nargs=1 -complete=customlist,NoteComplete Note call EditFile(my_notes_dir, <f-args>, '.markdown')
function! NoteComplete(ArgLead, CmdLine, CursorPos)
    return CompleteSpecialDir(g:my_notes_dir, '.markdown', a:ArgLead)
endfunction
" Outline
silent command! -nargs=1 -complete=customlist,OtlComplete Outline call EditFile(my_notes_dir, <f-args>, '.otl')
function! OtlComplete(ArgLead, CmdLine, CursorPos)
    return CompleteSpecialDir(g:my_notes_dir, '.otl', a:ArgLead)
endfunction
" }}}
" Task command and autocomplete {{{
let g:my_tasks_dir='~/dbox/tasks/'
silent command! -nargs=1 -complete=customlist,TaskComplete Task call EditFile(my_tasks_dir, <f-args>, '.quicktask')
function! TaskComplete(ArgLead, CmdLine, CursorPos)
    return CompleteSpecialDir(g:my_tasks_dir, '.quicktask', a:ArgLead)
endfunction
" }}}
" Prj Project command and autocomplete {{{
"
let g:my_projects_dir='~/dbox/projects/'
" Prj command autocompletes with a customlist from ProjectComplete function
silent command! -nargs=1 -complete=customlist,ProjectComplete Prj call LaunchProject(my_projects_dir, <f-args>, '.vimproject')
" custom complete function to return .vimproject files in the projects dir
function! ProjectComplete(ArgLead, CmdLine, CursorPos)
    return CompleteSpecialDir(g:my_projects_dir, '.vimproject', a:ArgLead)
endfunction
" open project plugin with the specified project file
function! LaunchProject(dir, basename, extension)
     execute 'Project '.a:dir.a:basename.a:extension
endfunction
" }}}
" Startify settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! Filter_header(lines) abort
    let longest_line   = max(map(copy(a:lines), 'len(v:val)'))
    let centered_lines = map(copy(a:lines),
        \ 'repeat(" ", (&columns / 2) - (longest_line / 2)) . v:val')
    return centered_lines
endfunction

let g:ascii = [
      \ '        __',
      \ '.--.--.|__|.--------.',
      \ '|  |  ||  ||        |',
      \ ' \___/ |__||__|__|__|']
      "\ '']
"let g:startify_custom_header = g:ascii + startify#fortune#boxed()
let g:startify_custom_header = 'Filter_header(g:ascii)'
"let g:startify_custom_header = 'Filter_header(map(g:ascii + startify#fortune#boxed(), "\"   \".v:val"))'
"
" show environment variables in file paths if the var name is shorter
let g:startify_use_env = 1

"let g:startify_change_to_dir = $HOME.'/dbox/notes'
let g:startify_list_order = [
    \ ['Projects'], 'commands',
    \ ['Recent'], 'files',
    \ ['Recent Notes'], 'dir',
    \ ['Bookmarks'], 'bookmarks',
    \ ['Sessions'], 'sessions',
    \ ]

let g:startify_commands = [
    \ {'a': ['aura', 'Project ~/dbox/projects/aura.vimproject']},
    \ {'l': ['laf', 'Project ~/dbox/projects/laf.vimproject']},
    \ {'m': ['main', 'Project ~/dbox/projects/main.vimproject']},
    \ {'p': ['206/patch', 'Project ~/dbox/projects/206patch.vimproject']},
    \ ]
let g:startify_files_number = 5


"let g:startify_commands =
"let g:startify_
"let g:startify_
"let g:startify_

"}}}
" helper functions {{{
"
function! EditFile(dir, basename, extension)
     execute 'e '.a:dir.a:basename.a:extension
endfunction
"
" customList command completion function to return search matches to the root filenames in a
" specified directory
"
" param: dir - the target directory
" param: extension - the file extension to return
" param: searchTerm - the term to match
" returns: list of base filenames in the specified directory which match the searchTerm and have the
" specified file extension.
"
function! CompleteSpecialDir(dir, extension, searchTerm)
    " glob - builds a \n separated string of matching files
    " split separates it into a list
    return MapPathListToFileRoot(split(glob(a:dir.a:searchTerm.'*'.a:extension), "\n"))
endfunction

"
" Given a list of file paths, returns a list containing just the base filenames, with any spaces
" in the filename escaped with '\ '
"
function! MapPathListToFileRoot(pathlist)
    " map modifies the paths to list just the root name
    " substitute escapse spaces inside the filename - need single quotes for expansion, so needed to
    " double them to escape in the eval string passed to map
    " fnamemodify takes the full path and replaces it with just the filename (:t) and just the root
    " without the file extension (:r)
    return map(a:pathlist, 'substitute(fnamemodify(v:val, '':t:r''),''\ '', ''\\\ '', "g")')
endfunction

" }}}

" }}}
" DISABLED PLUGINS {{{
" Pad settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:pad#dir = "~/dbox/notes"
"let g:pad#window_height = 15
"let g:pad#default_file_extension = ".md"
"let g:pad#default_format = "mkd"
"let g:pad#open_in_split = 0
"let g:pad#search_backend = "ack"
"
" to list pads on the right
"
"let g:pad#position = { "list": "right", "pads": "right" }
"let g:pad#window_width = 100

"}}}
" Quicktask settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"let g:quicktask_snip_path = "~/notes/"
"let g:quicktask_snip_default_filetype = "mkd"
" save on loss of focus or buffer switch
"let g:quicktask_autosave = 1
" don't add line for @Added [DATE] - might be more useful to just conceal these lines
"let g:quicktask_task_insert_added = 0
" don't include time with date added. defaults to 0/false. Explicitly included here in case I want
" to track times for tasks
"let g:quicktask_task_added_include_time = 0

"}}}
" ShowTrailingWhitespace settings {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" defaults below
"highlight ShowTrailingWhitespace ctermbg=Red guibg=Red
"let g:ShowTrailingWhitespace = 1

"}}}
" Javascript {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Define jquery syntax so jquery.vim plugin works
"au BufRead,BufNewFile *.js set ft=javascript syntax=javascript

" enable syntax highlighting of html and css in javascript
"let javascript_enable_domhtmlcss=1


"}}}
" ShowMarks {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ShowMarks plugin displays a left column labeling the marks in the current file
"
" <leader>mt toggles display of the column
" <leader>mm mark current line with next available lowercase letter
"
" disable showmarks by default. display as needed with toggle
let g:showmarks_enable = 0


"}}}
" MiniBufExpl: {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:miniBufExplBRSplit = 0
"let g:miniBufExplBuffersNeeded = 0
"let g:miniBufExplHideWhenDiff = 1
"let g:miniBufExplForceSyntaxEnable = 1
"noremap <C-t><C-t> :MBEFocus<cr>
"noremap <C-t>j   :bp<cr>
"noremap <C-t>k   :bn<cr>
"noremap <C-S-J>   :MBEbp<cr>
"noremap <C-S-K>   :MBEbn<cr>
" MiniBufExpl Colors
"hi MBENormal               guifg=#808080 guibg=fg
"hi MBEChanged              guifg=#CD5907 guibg=fg
"hi MBEVisibleNormal        guifg=#5DC2D6 guibg=fg
"hi MBEVisibleChanged       guifg=#F1266F guibg=fg
"hi MBEVisibleActiveNormal  guifg=#A6DB29 guibg=fg
"hi MBEVisibleActiveChanged guifg=#F1266F guibg=fg


"}}}
" IndentLine{{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" IndentLine:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" renders vertical line indentation indicators
let g:indentLine_bufNameExclude = ['*\.vimprojects']
"let g:indentLine_color_term = 239
"let g:indentLine_color_gui = '#09AA08'
"let g:indentLine_char = '│'
let g:indentLine_char = '┊'


"}}}
" Sparkup {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sparkup: - zencoding
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
" let g:sparkupExecuteMapping='<c-e>'

" Bundle 'Lokaltog/vim-easymotion'
" use <leader><leader>w to activate


"}}}
" CtrlP {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" CtrlP:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:ctrlp_working_path_mode = 'a'
let g:ctrlp_switch_buffer = 0 " disabled. was 2: don't switch to existing buffer unless on same tab
let g:ctrlp_custom_ignore = {'dir': 'node_modules$\|dist$\|build$'}
let g:ctrlp_arg_map = 1 " <c-o> or <c-y> accept key arg for where to open: thvr
let g:ctrlp_open_multiple_files = '4vr' " use <c-z> to select and <c-o> to open mult in v-splits

"fooo | bar | baz
"a| b| c




"}}}
" Powerline {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" POWERLINE:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:Powerline_symbols = 'unicode'
let g:Powerline_symbols = 'fancy'

"}}}
" NERDTree {{{
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree:
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:NERDTreeChDirMode=1 " changes vim cwd with :NERDTree [path]. this scopes fuzzy finder and ack correctly


"}}}
"}}}
"
" vimline fold options - used to toggle the defaults between when actively
" working on this file vs reference/small tweaks
"
":foldmethod=marker:foldlevel=0:foldmarker='{{{,}}}'
"
":foldmethod=marker foldlevel=0
"}}}
"}}}
"}}}

":foldmethod=marker:foldlevel=0:foldmarker='{{{,}}}'
"
":foldmethod=marker foldlevel=0
" vim:foldmethod=marker
